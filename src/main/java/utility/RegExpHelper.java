package utility;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpHelper {

    public static List<String> getValues(String text, String regexp) {
        List<String> results = new ArrayList<>();
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            for (int i = 1; i <= matcher.groupCount(); i++) {
                results.add(matcher.group(i));
            }
        }
        return results;
    }

    public static String getFirstGroup(String text, String regexp) {
        return getValues(text, regexp).get(0);
    }

}
