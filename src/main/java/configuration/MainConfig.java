package configuration;

import org.aeonbits.owner.Config;

@Config.Sources("classpath:config.properties")
public interface MainConfig extends Config {

    @Key("base.url")
    String getBaseUrl();

}
