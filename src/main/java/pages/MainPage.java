package pages;

import com.codeborne.selenide.Selenide;

public class MainPage {

    public MainPage openPage() {
        Selenide.open("/");
        return this;
    }

    public MainPage closeGeoPopup() {
        Selenide.$("#geo-popup-close")
                .click();
        return this;
    }

}
