package pages.widgets;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class PaginationWidget {

    SelenideElement
            pageSizeForm = $("#page_size_form1"),
            nextPageButton = $x("//div[@class='paginator']/a[@class='b']");

    public void choosePerPageOption(int index) {
        pageSizeForm.$x("//div[@class='input fl']").click();
        pageSizeForm.$$x("//span[@data-pagesize]").get(index).click();
    }

    public void clickNextPage() {
        nextPageButton
                .click();
    }

    public boolean isNextPagePresent() {
        return nextPageButton.exists();
    }

}
