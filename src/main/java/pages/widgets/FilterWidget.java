package pages.widgets;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import utility.RegExpHelper;

import java.util.Optional;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$x;

public class FilterWidget {


    /**
     * Returns amount of items in selected category
     *
     * @param itemsAmount
     * @return
     */
    public Integer selectCategoryWithProductsAmountAbove(Integer itemsAmount) {
        ElementsCollection categories = $$x("//div[@class='ch_item wh on_filter' and ancestor::*[6]/div/span[text()='Категория']]");
        Optional<SelenideElement> category = categories.stream().filter(e -> getCategoryItemsAmount(e) > itemsAmount).findFirst();
        if (!category.isPresent())
            throw new IllegalArgumentException(String.format("There is no category with items more than %d", itemsAmount));
        SelenideElement categoryElement = category.get();
        categoryElement.click();
        return getCategoryItemsAmount(categoryElement);
    }

    public void submitFilter() {
        $("#submitfilter").click();
    }

    private Integer getCategoryItemsAmount(SelenideElement element) {
        return new Integer(RegExpHelper.getFirstGroup(element.getText(), "(\\d+)"));
    }
}
