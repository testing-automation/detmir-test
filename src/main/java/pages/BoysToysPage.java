package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import pages.widgets.FilterWidget;
import pages.widgets.PaginationWidget;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$x;

public class BoysToysPage {

    private PaginationWidget paginationWidget = new PaginationWidget();
    private FilterWidget filterWidget = new FilterWidget();

    public BoysToysPage openPage() {
        Selenide.open("/catalog/index/name/boys_games/");
        return this;
    }

    public PaginationWidget paginationWidget() {
        return paginationWidget;
    }

    public FilterWidget filterWidget() {
        return filterWidget;
    }

    public List<Integer> getProductsIdsFromCurrentPage() {
        List<Integer> ids = new ArrayList<>();
        ElementsCollection products = $$x("//div[@rrproductid and @data-code]");
        for (SelenideElement product : products) {
            ids.add(new Integer(product.getAttribute("rrproductid")));
        }
        return ids;
    }

    public List<Integer> getProductIdsFromCurrentToLastPage() {
        List<Integer> productsIds = getProductsIdsFromCurrentPage();
        while (paginationWidget.isNextPagePresent()) {
            paginationWidget.clickNextPage();
            productsIds.addAll(getProductsIdsFromCurrentPage());
        }
        return productsIds;
    }
}
