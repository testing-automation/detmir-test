package tests.catalog.pagination;

import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;
import pages.BoysToysPage;
import pages.widgets.PaginationWidget;
import tests.AbstractBrowserTest;

import java.util.List;

public class BoysToysPaginationTests extends AbstractBrowserTest {

    BoysToysPage boysToysPage = new BoysToysPage();

    @Test
    public void itemsListImmutableToItemPerPageValue() {
        PaginationWidget widget = boysToysPage.paginationWidget();
        boysToysPage.openPage();

        Integer itemsAmount = boysToysPage
                .filterWidget()
                .selectCategoryWithProductsAmountAbove(80);
        boysToysPage.filterWidget().submitFilter();

        widget.choosePerPageOption(0);
        List<Integer> listA = boysToysPage.getProductIdsFromCurrentToLastPage();

        widget.choosePerPageOption(1);
        List<Integer> listB = boysToysPage.getProductIdsFromCurrentToLastPage();

        widget.choosePerPageOption(2);
        List<Integer> listC = boysToysPage.getProductIdsFromCurrentToLastPage();

        Assertions.assertThat(listA.size()).as("Items amount should be " + itemsAmount).isEqualTo(itemsAmount);
        Assertions.assertThat(listA.toArray()).as("Items list with per_page=20 should be equal items list with per_page=40").containsExactlyInAnyOrder(listB.toArray());
        Assertions.assertThat(listB.toArray()).as("Items list with per_page=40 should be equal items list with per_page=80").containsExactlyInAnyOrder(listC.toArray());
    }

}
