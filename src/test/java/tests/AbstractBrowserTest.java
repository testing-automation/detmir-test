package tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import configuration.MainConfig;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import pages.MainPage;

public class AbstractBrowserTest {

    MainConfig config = ConfigFactory.create(MainConfig.class);

    @BeforeSuite
    public void setupBrowser() {
        Configuration.browser = "chrome";
        Configuration.baseUrl = config.getBaseUrl();
        Configuration.startMaximized = true;
        WebDriverRunner.setWebDriver(createChromeDriver());
        openIndexPageAndCloseGeoPopup();
    }

    private ChromeDriver createChromeDriver() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--disable-notifications");
        chromeOptions.addArguments("start-maximized");
        return new ChromeDriver(chromeOptions);
    }

    private void openIndexPageAndCloseGeoPopup() {
        MainPage mainPage = new MainPage();
        mainPage.openPage()
                .closeGeoPopup();
    }

    @AfterSuite
    public void closeBrowserAfterTestClass() {
        WebDriverRunner.closeWebDriver();
    }


}
